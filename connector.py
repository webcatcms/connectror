from PyQt5 import QtCore, QtGui, QtWidgets
import sys
import os
import signal
import logging
import subprocess
import psycopg2
import pickle
import struct
from sshtunnel import *
import winreg
from time import sleep
import netifaces
from win10toast import ToastNotifier
from main_windows import Ui_MainWindow
class Connector(QtWidgets.QMainWindow):

    def __init__(self, *args, **kwargs):
        super(Connector, self).__init__(*args, **kwargs)

        # Clear log if exist
        if os.path.isfile('logfile.log'):
            os.remove('logfile.log')

        # get parameters in register
        location = winreg.HKEY_LOCAL_MACHINE
        try:
            soft = winreg.OpenKeyEx(location, "SOFTWARE\\Connector\\")
            self.db_name = winreg.QueryValueEx(soft, "db_name")[0]
            self.db_username = winreg.QueryValueEx(soft, "db_username")[0]
            self.db_password = winreg.QueryValueEx(soft, "db_password")[0]
            self.db_port = winreg.QueryValueEx(soft, "db_port")[0]
            self.ssh_hostname = winreg.QueryValueEx(soft, "ssh_hostname")[0]
            self.ssh_username = winreg.QueryValueEx(soft, "ssh_username")[0]
            self.ssh_password = winreg.QueryValueEx(soft, "ssh_password")[0]
            self.ssh_port = winreg.QueryValueEx(soft, "ssh_port")[0]
            self.api_port = winreg.QueryValueEx(soft, "api_port")[0]
        except:
            soft = winreg.OpenKeyEx(location, "SOFTWARE\\")
            key = winreg.CreateKey(soft, "Connector")
            winreg.SetValueEx(key, "db_name", 0, winreg.REG_SZ, "db_name")
            winreg.SetValueEx(key, "db_username", 0, winreg.REG_SZ, "postgres")
            winreg.SetValueEx(key, "db_password", 0, winreg.REG_SZ, "postgres")
            winreg.SetValueEx(key, "db_port", 0, winreg.REG_SZ, "5432")
            winreg.SetValueEx(key, "ssh_hostname", 0, winreg.REG_SZ, "192.168.0.1")
            winreg.SetValueEx(key, "ssh_username", 0, winreg.REG_SZ, "")
            winreg.SetValueEx(key, "ssh_password", 0, winreg.REG_SZ, "")
            winreg.SetValueEx(key, "ssh_port", 0, winreg.REG_SZ, "22")
            winreg.SetValueEx(key, "api_port", 0, winreg.REG_SZ, "7788")
            if key:
                winreg.CloseKey(key)
            soft = winreg.OpenKeyEx(location, "SOFTWARE\\Connector\\")
            self.db_name = winreg.QueryValueEx(soft, "db_name")[0]
            self.db_username = winreg.QueryValueEx(soft, "db_username")[0]
            self.db_password = winreg.QueryValueEx(soft, "db_password")[0]
            self.db_port = winreg.QueryValueEx(soft, "db_port")[0]
            self.ssh_hostname = winreg.QueryValueEx(soft, "ssh_hostname")[0]
            self.ssh_username = winreg.QueryValueEx(soft, "ssh_username")[0]
            self.ssh_password = winreg.QueryValueEx(soft, "ssh_password")[0]
            self.ssh_port = winreg.QueryValueEx(soft, "ssh_port")[0]
            self.api_port = winreg.QueryValueEx(soft, "api_port")[0]


        self.conn_error = False

        # set properties value
        self.conn = self.server = self.conn_db = self.cursor = self.process = None


        if getattr(sys, 'frozen', False):
            self.icon = os.path.join(sys._MEIPASS, "medoc.ico")
        else:
            self.icon = "medoc.ico"

        self.isAlive = True

        self.toaster = ToastNotifier()

        self.initUI()
        self.main()


    def get_config(self, edrpo):

        # get config file size and login pass
        try:
            result = {"get_config": edrpo}
            message_to_send = pickle.dumps(result)
            self.conn.sendall(struct.pack('>I', len(message_to_send)))
            self.conn.sendall(message_to_send)

            data_size = struct.unpack('>I', self.conn.recv(4))[0]
            received_payload = b""
            reamining_payload_size = data_size
            while reamining_payload_size != 0:
                received_payload += self.conn.recv(reamining_payload_size)
                reamining_payload_size = data_size - len(received_payload)
            data = pickle.loads(received_payload)


            if 'config' in data:
                str_pass = data['login'].replace(' ', '') + '\n' + data['password']
                try:
                    with open('pass.txt', 'w') as p:
                        p.write(str_pass)

                    data = data['config'].decode("utf-8").replace('auth-user-pass', 'auth-user-pass pass.txt')

                    with open('config.ovpn', "w") as f:
                        f.write(data)

                    return True
                except Exception as e:
                    return False
            else:
                return False

        except Exception as e:
            self.check_connection()

    def connect_vpn(self, org):

        if self.get_config(org[1]):

            openvpn_gui_path = r"C:\Program Files\OpenVPN\bin\openvpn.exe"
            command = f'"{openvpn_gui_path}"  --config config.ovpn'

            self.process = subprocess.Popen(command, shell=True)

            i = 0
            while i < 3:
                i = netifaces.ifaddresses('OpenVPN Data Channel Offload').__len__()
                sleep(3)
            self.toaster.show_toast("Зєднання встановлено", org[0], duration=10, icon_path=self.icon, threaded=True)


    def disconnect_vpn(self, org):

        subprocess.run(f"taskkill /f /im openvpn.exe", shell=True)

        i = netifaces.ifaddresses('OpenVPN Data Channel Offload').__len__()

        while i > 2:
            i = netifaces.ifaddresses('OpenVPN Data Channel Offload').__len__()
            sleep(3)
        self.toaster.show_toast("Зєднання розірвано", org[0], duration=10, icon_path=self.icon, threaded=True)


    def check_connection(self):
        try:
            # Connection DB
            if self.conn_db == None:
                try:
                    self.conn_db = psycopg2.connect(
                        database=self.db_name,
                        user=self.db_username,
                        password=self.db_password,
                        host='localhost',
                        port=int(self.db_port))
                    self.cursor = self.conn_db.cursor()
                except Exception as e:
                    raise e


            # ssh tunnel
            if self.server == None:
                try:
                    self.server = open_tunnel(
                        (self.ssh_hostname, int(self.ssh_port)),
                        ssh_username=self.ssh_username,
                        ssh_password=self.ssh_password,
                        remote_bind_address=('127.0.0.1', int(self.api_port)))
                    self.server.start()
                except Exception as e:
                    raise e

            # Create socket connect
            try:
                self.conn = None
                self.conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.conn.connect(('localhost', self.server.local_bind_port))
                message_to_send = pickle.dumps({})
                self.conn.sendall(struct.pack('>I', len(message_to_send)))
                self.conn.sendall(pickle.dumps({}))
            except Exception as e:
                raise e


        except Exception as e:
            self.conn_error = True

    def main(self):

        self.check_connection()

        while self.isAlive:
            if self.conn_error:
                self.check_connection()
            else:
                self.cursor.execute(
                    'select le.idorg as in ,le.datetime as datetime_in, le2.idorg as out from LOGSEVENT le left join LOGSEVENT le2 on (le.idorg = le2.idorg and le.datetime < le2.datetime) where le.code = %s order by le.datetime desc  limit 1',
                    ("InEnterprise",))
                result = self.cursor.fetchone()
                in_id = result[0]
                datetime_in = result[1]
                out_id = result[2]

                if out_id == None:
                    self.cursor.execute('SELECT name, edrpou  FROM org WHERE code = %s', (in_id,))
                    org = self.cursor.fetchone()
                    self.connect_vpn(org)
                    is_live = True

                    while is_live:
                        self.cursor.execute('select idorg from LOGSEVENT where code = %s and idorg = %s and datetime > %s ',
                                       ("OutEnterprise", in_id, datetime_in))
                        out_id = self.cursor.fetchone()
                        if out_id != None:
                            self.disconnect_vpn(org)
                            is_live = False
                        sleep(5)
            sleep(5)

    def initUI(self):
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.setWindowIcon(QtGui.QIcon("medoc.ico"))
        self.setIconSize(QtCore.QSize(32, 32))

        # Init QSystemTrayIcon
        self.tray_icon = QtWidgets.QSystemTrayIcon(self)
        self.tray_icon.setIcon(QtGui.QIcon("medoc.ico"))

        # Tray menu
        # show_action = QtWidgets.QAction("Show", self)
        quit_action = QtWidgets.QAction("Exit", self)
        # hide_action = QtWidgets.QAction("Hide", self)
        # show_action.triggered.connect(self.show1)
        # hide_action.triggered.connect(self.hide)
        quit_action.triggered.connect(app.quit)

        tray_menu = QtWidgets.QMenu()
        # tray_menu.addAction(show_action)
        # tray_menu.addAction(hide_action)
        tray_menu.addAction(quit_action)
        self.tray_icon.setContextMenu(tray_menu)
        self.tray_icon.show()

        # self.show()

    def show1(self):
        pass



if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    app.setQuitOnLastWindowClosed(False)
    ex = Connector()
    sys.exit(app.exec_())